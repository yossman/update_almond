@extends('voyager::master')
@section('content')
<link rel="stylesheet" href="{{url('css/styles.css')}}">
      @include('voyager::alerts')
        <div class="container">
              <div class="admin-section-title">
                  <h3><i class="voyager-people"></i> {{ __('District Table') }}</h3>
              </div><div class="btn-group float-right">
                    <button class="btn btn-success" type="button" id="btn_add">Add District</button>
                  </div>
                       <table class="table table-hover dataTable no-footer">
                              <thead>
                                <tr>
                                  <th>ID</th>
                                  <th>Name</th>
                                </tr>
                               </thead>
                               <tbody id="students-list" name="students-list">
                                 @foreach ($districts as $district)
                                  <tr id="district{{$district->id}}">
                                    <td>{{$district->id}}</td>
                                    <td>{{$district->name}}</td>
                                  </tr>
                              @endforeach
                          </tbody>
                        </table>
                     </div>
                       <div class="container add-panel">
                         <div class="panel panel-success">
                           <div class="panel-heading" style="background-color: #1abc9c;">
                               <h2 class="panel-title">Add District Data</h2>
                          </div>
                          <br>
                          <div class="panel-body">
                         <form action="districts" method="POST">
                           {{csrf_field()}}
                           <div class="form-group">
                             <label for="name">Name :</label>
                             <input type="text" name="name" class="form-control" required>
                           </div>
                           <div class="form-group">
                             <button type="submit" class="btn btn-primary"> Submit </button>
                           </div>
                         </form>
                       </div>
                      </div>
                    </div>
                  </div>
              <div class="clear"></div>
            @include('layouts.errors')
            </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="js/users.js"></script>
<script>
var $state = $('#user_profile'), $province = $('#franchise');
$state.change(function () {
    if ($state.val() == 2) {
      $province.removeAttr('disabled');
    } else {
      $province.attr('disabled', 'disabled').val('');
    }
}).trigger('change');
</script>
@endsection
