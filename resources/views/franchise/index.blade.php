@extends('voyager::master')
@section('content')
<link rel="stylesheet" href="{{url('css/styles.css')}}">
      @include('voyager::alerts')
        <div class="container">
              <div class="admin-section-title">
                  <h3><i class="voyager-people"></i> {{ __('Franchise Table') }}</h3>
              </div><div class="btn-group float-right">
                    <button class="btn btn-success" type="button" id="btn_add">Add Franchise</button>
                  </div>
                       <table class="table table-hover dataTable no-footer">
                              <thead>
                                <tr>
                                  <th>ID</th>
                                  <th>Name</th>
                                  <th>Franchise</th>
                                  <th>Mobile No. </th>
                                  <th>Email</th>
                                  <th>Tree</th>
                                </tr>
                               </thead>
                               <tbody id="students-list" name="students-list">
                                 @foreach ($students as $student)
                                  <tr id="student{{$student->id}}">
                                   <td>{{$student->id}}</td>
                                   <td>{{$student->name}}</td>
                                   <td>{{$student->franchise}}</td>
                                   <td>{{$student->mob_no}}</td>
                                   <td>{{$student->email}}</td>
                                   <td><a class="btn btn-info no-underline" href="{{url('students/tree/'.$student->id)}}">View Tree</a></td>
                                    <!-- <td>
                                    <button class="btn btn-warning btn-email open_modal" value="{{$student->id}}">Edit</button>
                                    <button class="btn btn-danger btn-delete delete-student" value="{{$student->id}}">Delete</button>
                                  </td> -->
                                  </tr>
                              @endforeach
                          </tbody>
                        </table>
                     </div>
                       <div class="container add-panel">
                         <div class="panel panel-success">
                           <div class="panel-heading" style="background-color: #1abc9c;">
                               <h2 class="panel-title">Add Franchise Data</h2>
                          </div>
                          <br>
                          <div class="panel-body">
                         <form action="students" method="POST">
                           {{csrf_field()}}
                           <div class="form-group">
                             <label for="name">Name :</label>
                             <input type="text" name="name" class="form-control" required>
                           </div>
                           @if(Auth::check())
                           <input type="hidden" name="parent_id" class="form-control" value="{{auth()->user()->id}}">
                           @endif
                           <div class="form-group">
                             <label for="email">Email :</label>
                             <input type="email" name="email" class="form-control" required>
                           </div>
                           <div class="form-group">
                             <label for="address">Address :</label>
                             <textarea name="address" class="form-control" required></textarea>
                           </div>
                           <div class="row">
                             <div class="col-md-12">
                               <div class="col-md-6">
                                 <div class="form-group">
                                   <label for="gender">Gender :</label>
                                   <input type="text" name="gender" class="form-control" required>
                                 </div>
                               </div>
                               <div class="col-md-6">
                                 <div class="form-group">
                                   <label for="mob_no">Mob No :</label>
                                   <input type="number" name="mob_no" class="form-control">
                                 </div>
                               </div>
                             </div>
                           </div>
                           <div class="row">
                             <div class="col-md-12">
                               <div class="col-md-6">
                                 <div class="form-group">
                                   <label for="password">Password :</label>
                                   <input type="password" name="password" class="form-control" required>
                                 </div>
                               </div>
                               <div class="col-md-6">
                                 <div class="form-group">
                                   <label for="password_confirmation">Retype Password :</label>
                                   <input type="password" name="password_confirmation" class="form-control" required>
                                 </div>
                               </div>
                             </div>
                           </div>
                           @if(auth()->user()->role_id == "1")
                           <div class="form-group">
                             <label for="user_profile">User Profile :</label>
                             <select name="user_profile" id="user_profile" class="form-control">
                                <option selected>Open this select menu</option>
                                <option value="2">User</option>
                                <option value="3">Franchise</option>
                              </select>
                           </div>
                          @else
                          <input type="hidden" name="user_profile" value="3">
                          @endif
                          @if(auth()->user()->role_id === 3)
                          <input type="hidden" name="franchise" value="3">
                          @else
                          <div class="form-group">
                            <label for="franchise">Franchise :</label>
                            <select name="franchise" class="form-control">
                              <option selected>Select Franchise</option>
                              @foreach($franchise as $franchis)
                               <option value="3">{{$franchis->name}}</option>
                               @endforeach
                             </select>
                          </div>
                           @endif
                             <div class="form-group">
                               <label for="district">District :</label>
                               <select name="district" class="form-control">
                                  <option selected>Open this select menu</option>
                                  <option value="1">One</option>
                                  <option value="2">Two</option>
                                  <option value="3">Three</option>
                                </select>
                             </div>
                           <div class="form-group">
                             <button type="submit" class="btn btn-primary"> Submit </button>
                           </div>
                         </form>
                       </div>
                      </div>
                    </div>
                  </div>
              <div class="clear"></div>
            @include('layouts.errors')
            </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="js/users.js"></script>
<script>
var $state = $('#user_profile'), $province = $('#franchise');
$state.change(function () {
    if ($state.val() == 2) {
      $province.removeAttr('disabled');
    } else {
      $province.attr('disabled', 'disabled').val('');
    }
}).trigger('change');
</script>
@endsection
