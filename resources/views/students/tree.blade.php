@extends('voyager::master')

@section('page_title', __('voyager.generic.media'))
@section('content')
<link rel="stylesheet" href="{{url('css/tree.css')}}">
<div class="page-content container-fluid">
    @include('voyager::alerts')
      <div class="row">
        <div class="col-md-12 tree">
            <div class="admin-section-title">
              <h3><i class="voyager-tree"></i> {{ __('Tree Section For') }}</h3>
            </div>
            <div class="container">
              {!!$user!!}
            </div>
          </div>
        </div>
</div>
@endsection
