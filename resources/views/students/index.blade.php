@extends('voyager::master')
@section('css')
<link rel="stylesheet" href="{{url('css/styles.css')}}">
<style>.profile{display:none !important;}</style>
@endsection
@section('page_title', __('Students'))
@section('content')
        @include('voyager::alerts')
        <div class="container">
                <div class="admin-section-title">
                    <h3><i class="voyager-people"></i> {{ __('Users Table') }}</h3>
                </div>
                    <div class="btn-group float-right">
                    <!--   <a href="admin/users/create">-->
                   <button class="btn btn-success" type="button" id="btn_add">Add User</button>
                    <!--<button class="btn btn-success" type="button">Add User</button>-->
                  </a>
                    </div>
                         <table class="table-hover dataTable no-footer">
                                <thead>
                                  <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Franchise</th>
                                    <th>Mobile No. </th>
                                    <th>Email</th>
                                    <th>Tree</th>
                                  </tr>
                                 </thead>
                                 <tbody id="students-list" name="students-list">
                                   @foreach ($students as $student)
                                    <tr id="student{{$student->id}}">
                                     <td>{{$student->id}}</td>
                                     <td>{{$student->name}}</td>
                                     @if(auth()->user()->role_id === 3)
                                     <td>Current</td>
                                     @else
                                      @if(is_numeric($student->franchise))
                                        @foreach($franchise as $franch)
                                          @if($franch->id == $student->franchise)
                                            <td>{{$franch->name}}</td>
                                          @endif
                                        @endforeach
                                      @else
                                        <td>Empty</td>
                                      @endif
                                     @endif
                                     <td>{{$student->mob_no}}</td>
                                     <td>{{$student->email}}</td>
                                     <td><a class="btn btn-info no-underline" href="{{url('students/tree/'.$student->id)}}">View Tree</a></td>
                                      <!-- <td>
                                      <button class="btn btn-warning btn-email open_modal" value="{{$student->id}}">Edit</button>
                                      <button class="btn btn-danger btn-delete delete-student" value="{{$student->id}}">Delete</button>
                                    </td> -->
                                    </tr>
                                  @endforeach
                            </tbody>
                          </table>
                         <div class="container add-panel">
                           <div class="panel panel-success">
                             <div class="panel-heading" style="background-color: #1abc9c;">
                                 <h2 class="panel-title">Add Student Data</h2>
                            </div>
                            <br>
                            <div class="panel-body">
                           <form action="students" method="POST">
                             {{csrf_field()}}
                             <div class="form-group">
                               <label for="name">Name :</label>
                               <input type="text" name="name" class="form-control" required>
                             </div>
                             <div class="form-group">
                               <label for="email">Email :</label>
                               <input type="email" name="email" class="form-control" required>
                             </div>
                             <div class="form-group">
                               <label for="address">Address :</label>
                               <textarea name="address" class="form-control" required></textarea>
                             </div>
                             <div class="row">
                               <div class="col-md-12">
                                 <div class="col-md-6">
                                   <div class="form-group">
                                     <label for="gender">Gender :</label>
                                     <input type="text" name="gender" class="form-control" required>
                                   </div>
                                 </div>
                                 <div class="col-md-6">
                                   <div class="form-group">
                                     <label for="mob_no">Mob No :</label>
                                     <input type="number" name="mob_no" class="form-control">
                                   </div>
                                 </div>
                               </div>
                             </div>
                             <div class="row">
                               <div class="col-md-12">
                                 <div class="col-md-6">
                                   <div class="form-group">
                                     <label for="password">Password :</label>
                                     <input type="password" name="password" class="form-control" required>
                                   </div>
                                 </div>
                                 <div class="col-md-6">
                                   <div class="form-group">
                                     <label for="password_confirmation">Retype Password :</label>
                                     <input type="password" name="password_confirmation" class="form-control" required>
                                   </div>
                                 </div>
                               </div>
                             </div>
                             @if(auth()->user()->role_id == "1")
                             <div class="form-group">
                               <label for="user_profile">User Profile :</label>
                               <select name="user_profile" class="form-control" id="user_profile">
                                  <option selected>Open this select menu</option>
                                  <option value="1">Admin</option>
                                  <option value="2">User</option>
                                  <option value="3">Franchise</option>
                                  <option value="4">Supervisor</option>
                                </select>
                             </div>

                            @else
                            <input type="hidden" name="user_profile" value="2">
                            @endif
                            @if(auth()->user()->role_id === 3)
                            <input type="hidden" name="franchise" value="{{auth()->user()->id}}">
                            @else
                             <div class="form-group">
                               <label for="franchise">Franchise :</label>
                               <select name="franchise" class="form-control" id="franchise">
                                 <option selected>Select Franchise</option>
                                 @foreach($franchise as $franchis)
                                  <option value="{{$franchis->id}}">{{$franchis->name}}</option>
                                  @endforeach
                                </select>
                             </div>
                             @endif
                             @if(auth()->user()->role_id === 3)
                             <input type="hidden" name="district" value="{{auth()->user()->district}}">
                             @else
                             <div class="form-group">
                                 <label for="district">District :</label>
                                 <select name="district" class="form-control">
                                    <option selected>Open this select menu</option>
                                    @foreach($districts as $district)
                                     <option value="{{$district->id}}">{{$district->name}}</option>
                                     @endforeach
                                  </select>
                               </div>
                              @endif
                             <div class="form-group">
                               <button type="submit" class="btn btn-primary"> Submit </button>
                             </div>
                           </form>
                         </div>
                        </div>
                      </div>
                    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="js/users.js"></script>
    <script>
    var $state = $('#user_profile'), $province = $('#franchise');
   var $su_province = $('#supervisor_id');
    $state.change(function () {
        if ($state.val() == 2 || $state.val() == 4) {
          $province.removeAttr('disabled');
        } else {
          $province.attr('disabled', 'disabled').val('');
        }
    }).trigger('change');
     /* $state.change(function () {
        if ($state.val() == 4) {
          $su_province.removeAttr('disabled');
        } else {
          $su_province.attr('disabled','disabled').val('');
        }
    }).trigger('change'); */
    </script>
@endsection
