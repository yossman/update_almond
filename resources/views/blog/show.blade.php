@extends('blog.master')

@section ('body')
<div class="blog-post">
  <h2 class="blog-post-title">{{ $post->title }}</h2>
  <p class="blog-post-meta">{{ $post->created_at->toFormattedDateString() }}</p>
  <p>{!! $html = preg_replace('/(<\/[^>]+?>)(<[^>\/][^>]*?>)/', '$1 $2', $post->body);
        $html = strip_tags($html); echo $html !!}</p>
  @if(count($post->comments))
  <ul class="list-group">
    @foreach($post->comments as $comment)
    <li class="list-group-item"><b>{{ $comment->created_at->diffForHumans() }} : </b>{{$comment->body}}</li>
    @endforeach
  </ul>
  @endif
    <br>
    <hr>
        <form method="POST" action="{{ url('blog/'.$post->id.'/comments') }}">
          {{ csrf_field() }}
          <label for="comment">Your Comment : </label>
          <br>
          <div class="form-group">
            <textarea class="form-control" name="body" value="Type Your Comment"></textarea>
          </div>
          <input type="hidden" name="posts_id" value="{{$post->id}}">
          <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
          <div class="form-group">
            <button class="btn btn-primary" type="submit">submit</button>
          </div>
        </form>
    <div class="card">
      @include('layouts.errors')
    </div>
  </ul>
</div><!-- /.blog-post -->
@endsection
