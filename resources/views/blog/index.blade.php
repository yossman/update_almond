@extends('blog.master')
@section('body')
@foreach($posts as $post)
<div class="blog-post">
  <a href="blog/{{$post->id}}"><h2 class="blog-post-title">{{ $post->title }}</h2></a>
  <p class="blog-post-meta">@if(auth()->check())
      {{auth()->user()->name}}
      @endif
      on {{ $post->created_at->toFormattedDateString() }}</p>
  <p>{!! $html = preg_replace('/(<\/[^>]+?>)(<[^>\/][^>]*?>)/', '$1 $2', $post->body);
        $html = strip_tags($html); echo $html  !!}</p>
  <a class="btn-danger" href="{{ url('posts/delete/'.$post->id) }}">Delete Post</a>
</div><!-- /.blog-post -->
<hr>
@endforeach
@endsection
