@extends('voyager::master')

@section('css')
<link rel="stylesheet" href="{{url('css/styles.css')}}">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/stefangabos/Zebra_Datepicker/dist/css/bootstrap/zebra_datepicker.min.css">
<style>.profile{display:none !important;}.Zebra_DatePicker_Icon_Wrapper{width: 50% !important;}.Zebra_DatePicker_Icon_Wrapper input{background-color: #fff !important;}</style>
@endsection
@section('page_title', __('Events'))
@section('content')
<div class="page-content container-fluid">
        @include('voyager::alerts')
                <div class="admin-section-title">
                    <h3><i class="voyager-people"></i> {{ __('Events Table') }}</h3>
                </div>
                  <div class="container">
                    <div class="btn-group float-right">
                      <button class="btn btn-success" type="button" id="btn_add">Add Event</button>
                    </div>
                    @if(count($events))
                         <table class="table">
                                <thead>
                                  <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Created By</th>
                                    <th>Event Date</th>
                                    <th>Remind</th>
                                  </tr>
                                 </thead>
                                 <tbody id="students-list" name="students-list">
                                   @foreach ($events as $event)
                                    <tr id="student{{$event->id}}">
                                     <td>{{$event->id}}</td>
                                     <td>{{$event->name}}</td>
                                     <td>{{$event->created_by}}</td>
                                     <td>{{$event->event_date}}</td>
                                       <!-- <td>
                                      <button class="btn btn-warning btn-email open_modal" value="{{$event->id}}">Edit</button>
                                      <button class="btn btn-danger btn-delete delete-student" value="{{$event->id}}">Delete</button>
                                    </td> -->
                                    <td>  <span class="addtocalendar atc-style-theme">
                                      <a class="atcb-link"><i class="fa fa-calendar"> </i> Remind </a>
                                        <var class="atc_event">
                                            <var class="atc_date_start">{{$event->event_date}}</var>
                                            <var class="atc_date_end">{{$event->event_end}}</var>
                                            <var class="atc_timezone">Asia/Kolkata</var>
                                            <var class="atc_title">{{$event->name}}</var>
                                            <var class="atc_description">{{$event->description}}</var>
                                            <var class="atc_location">{{$event->location}}</var>
                                            <var class="atc_organizer">Almond Academy</var>
                                            <var class="atc_organizer_email">info@almondacademy.com</var>
                                        </var>
                                     </span></td>
                                    </tr>
                                @endforeach
                            </tbody>
                          </table>
                        @endif
                       </div>
                       <div class="container">
                         <iframe src="https://calendar.google.com/calendar/embed?src=lostsharma%40gmail.com&ctz=Asia/Calcutta" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
                       </div>
                         <div class="container add-panel">
                           <div class="panel panel-success">
                             <div class="panel-heading" style="background-color: #1abc9c;">
                                 <h2 class="panel-title">Add Events Data</h2>
                            </div>
                            <br>
                            <div class="panel-body">
                           <form action="events" method="POST">
                             {{csrf_field()}}
                             <div class="form-group">
                               <label for="name">Name :</label>
                               <input type="text" name="name" class="form-control" required>
                             </div>
                             <input type="hidden" name="created_by" class="form-control" value="{{auth()->user()->name}}">
                             <div class="row">
                               <div class="col-md-12">
                                 <div class="col-md-6">
                                   <div class="form-group">
                                     <label for="event_date">Event Date :</label>
                                     <input type="text" name="event_date" id="datepicker" class="form-control" placeholder="Click to add Event Date" required>
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                   <div class="form-group">
                                     <label for="end_date">Event End Date :</label>
                                     <input type="text" name="end_date" id="datepicker2" class="form-control" placeholder="Click to add Event End Date" required>
                                    </div>
                                 </div>
                               </div>
                             </div>
                             <div class="form-group">
                               <label for="description">Event Description :</label>
                               <textarea class="form-control" name="description" rows="8" cols="80" placeholder="Enter Description Here">
                               </textarea>
                              </div>
                              <div class="form-group">
                                <label for="location">Event Location</label>
                                <input type="text" class="form-control" name="location" placeholder="Enter Location Here">
                              </div>
                             <div class="form-group">
                               <button type="submit" class="btn btn-primary"> Submit </button>
                             </div>
                           </form>
                         </div>
                        </div>
                      </div>
                    </div>
                  </div>
                <div class="clear"></div>
              @include('layouts.errors')
              </div>
              <br>
  @section('javascript')
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <script src="js/users.js"></script>
  <script src="https://cdn.jsdelivr.net/gh/stefangabos/Zebra_Datepicker/dist/zebra_datepicker.min.js"></script>
  <script type="text/javascript" src="http://addtocalendar.com/atc/1.5/atc.min.js"></script>
  <script type="text/javascript">
  var calendars = {};

  $(document).ready(function() {
    $('#datepicker').Zebra_DatePicker();
      $('#datepicker2').Zebra_DatePicker();
  });
  </script>
  @endsection
@endsection
