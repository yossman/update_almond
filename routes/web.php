<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
Use App\student;
Use App\User;


Route::get('/', function () {
    return view('welcome');
});

// show all
Route::get('students', 'UsersController@index');

// Add
Route::post('students','UsersController@create');

// User Tree
Route::get('students/tree/{user}','UsersController@tree');

// User Logout
Route::get('logout',function(){
  auth()->logout();
  return redirect('admin');
});

// Franchise
Route::get('franchise','FranchiseController@index');

// Supervisor
Route::get('supervisor','SupervisorController@index');
Route::post('supervisor','SupervisorController@create');

// District
Route::get('districts','DistrictController@index');
Route::post('districts','DistrictController@create');

// Events
Route::get('events','EventController@index');
Route::post('events','EventController@create');
// Find

// Posts
Route::get('posts','PostController@index');

// Blogs
Route::get('blog','PostController@blog');
Route::get('blog/{post}','PostController@show');

// Comments
Route::post('blog/{post}/comments','CommentsController@store');
// Route::get('student/{student_id?}',function($student_id){
//     $students = student::find($student_id);
//     return response()->json($students);
// });

// Add
// Route::post('student',function(Request $request){
//     $students = student::create($request->input());
//     return response()->json($students);
// });

// Add
// Route::post('student/create',function(Request $request){
//    student::create(request(['name','email','password']));
//    return redirect('/student');
// });

// Delete
// Route::delete('student/{student_id}',function($student_id){
//    $students = student::destroy($student_id);
//    return redirect('/student');
// });
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
