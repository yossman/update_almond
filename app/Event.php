<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model implements \MaddHatter\LaravelFullcalendar\Event
{
    protected $fillable = [
        'name','franchise','event_date','created_by','location','event_end','description'
    ];

    /**
     * Get the event's id number
     *
     * @return int
     */
    public function getId() {
		    return $this->id;
	    }

    /**
     * Get the event's title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->name;
    }

    /**
     * Is it an all day event?
     *
     * @return bool
     */
    public function isAllDay()
    {
        return (bool)$this->all_day;
    }

    /**
     * Get the start time
     *
     * @return DateTime
     */
    public function getStart()
    {
        return $this->event_date;
    }

    /**
     * Get the end time
     *
     * @return DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    public function getEventOptions()
   {
       return [
           'color' => $this->background_color,
     //etc
       ];
   }
}
