<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Comments;

class Post extends Model
{
  protected $fillable = ['body','user_id'];

  public function comments(){
    return $this->hasMany(Comment::class);
  }
  public function addComment($body){
    $this->comments()->create(compact('body'));
  }
}
