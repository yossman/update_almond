<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Nestable\NestableTrait;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;
    use NestableTrait;

    protected $parent = 'parent_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id','role_id','name','email','franchise','district','address','gender','mob_no','password','slug',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function children(){
      return $this->hasMany($this,'parent_id')->with('children');
    }
    public function _construct(){
     return $this->belongsTo('App\District', 'name');
    }
}
