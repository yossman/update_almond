<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
  protected $fillable = [
      'parent_id','name','role_id','email','franchise','district','address','gender','mob_no','password','slug',
  ];
}
