<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Supervisor extends Model
{
    protected $fillable = [
        'name','franchise',
    ];
public function authorize()
    {
        return true;
    }
 protected $table = "Supervisor";

}
