<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\District;
use App\Profile;

class SupervisorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(auth()->user()->role_id == "1")
      {
          $students = User::all()->where('role_id',4);
          $franchise = User::all()->where('role_id',3);
          $districts = District::all();
          return view('supervisor.index', compact('students','franchise','districts'));
      } else {
          $students = User::all()->where('franchise',auth()->user()->id);
          $franchise = User::all()->where('id',auth()->user()->franchise);
          $districts = District::all()->where('district',auth()->user()->district);
          return view('supervisor.index', compact('students','franchise','districts'));
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
              // User Add
              User::create(['parent_id'=>request('franchise'),
                            'name'=>request('name'),
                            'role_id'=>request('user_profile'),
                            'email'=>request('email'),
                            'franchise'=>request('franchise'),
                            'district'=>request('district'),
                            'address'=>request('address'),
                            'gender'=>request('gender'),
                            'slug'=>strtolower(str_replace(' ','-',request('name'))),
                            'course'=>request('course'),
                            'mob_no'=>request('mob_no'),
                            'password'=>request('password'),
                          ]);

              // Profile Add
              Profile::create(['parent_id'=>request('franchise'),
                            'name'=>request('name'),
                            'email'=>request('email'),
                            'franchise'=>request('franchise'),
                            'district'=>request('district'),
                            'address'=>request('address'),
                            'gender'=>request('gender'),
                            'slug'=>strtolower(str_replace(' ','-',request('name'))),
                            'course'=>request('course'),
                            'mob_no'=>request('mob_no'),
                            'password'=>request('password'),
                          ]);

              return redirect('supervisor');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
