<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;
use App\District;

class UsersController extends Controller
{
    protected $attributes = array(
    'mob_no' => 'none'
    );
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    if(auth()->check())
    {
    if(auth()->user()->role_id == "1")
    {
        $students = User::all()->where('role_id',2);
        $franchise = User::all()->where('role_id',3);
        $districts = District::all();
        return view('students.index', compact('students','franchise','districts'));
    }
      elseif(auth()->user()->role_id == "2")
    {
        $students = User::all()->where('id',auth()->user()->id)->where('role_id',2);
        $franchise = User::all()->where('id',auth()->user()->parent_id);
        $districts = District::all();
        return view('students.index', compact('students','franchise','districts'));
    }
    else
    {
        $students = User::all()->where('parent_id',auth()->user()->id)->where('role_id',2);
        return view('students.index', compact('students'));
    }
  } else {
    return redirect('/');
  }
}
/* public function supervisorView(){
		$students = User::all()->where('role_id', '==' , 4);
        	$franchise = User::all()->where('role_id',3);
        	$districts = District::all();
        	return view('students.index', compact('students','franchise','districts'));
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // $validate = $this->validate(request(), ['parent_id','name','email','franchise','district','address','gender','course','mob_no','password']);
        // if(!$validate){
        //   dd(request(['parent_id','name','email','franchise','district','address','gender','course','mob_no','password']));
        // }

        // User Add

        User::create(['parent_id'=>request('franchise'),
                      'name'=>request('name'),
                      'role_id'=>request('user_profile'),
                      'email'=>request('email'),
                      'franchise'=>request('franchise'),
                      'district'=>request('district'),
                      'address'=>request('address'),
                      'gender'=>request('gender'),
                      'slug'=>strtolower(str_replace(' ','-',request('name'))),
                      'course'=>request('course'),
                      'mob_no'=>request('mob_no'),
                      'password'=>request('password'),
                    ]);

        // Profile Add

        Profile::create(['parent_id'=>request('franchise'),
                      'name'=>request('name'),
                      'email'=>request('email'),
                      'franchise'=>request('franchise'),
                      'district'=>request('district'),
                      'address'=>request('address'),
                      'gender'=>request('gender'),
                      'slug'=>strtolower(str_replace(' ','-',request('name'))),
                      'course'=>request('course'),
                      'mob_no'=>request('mob_no'),
                      'password'=>request('password'),
                    ]);

        return redirect('students');
        // User::create([
        //     'name'=> request('name'),
        //     'email'=> request('email'),
        //     'address'=> request('address'),
        //     'gender'=> request('gender'),
        //     'mob_no'=> request('mob_no'),
        //     'parent_id' => request('parent_id'),
        //     'course'=> request('course'),
        //     'franchise'=> request('franchise'),
        //     'district'=> request('district'),
        //     'password'=> request('password')
        //   ]);
        //

    }

    public function tree(User $user){
      $user = User::parent($user->id)->renderAsHtml();
      return view('students.tree', compact('user'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
