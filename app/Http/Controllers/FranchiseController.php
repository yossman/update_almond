<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class FranchiseController extends Controller
{
    public function index(){
      $franchise = User::all()->where('role_id',3);
      if(auth()->user()->role_id == 3)
      {
        $students = User::all()->where('parent_id',auth()->user()->id)->where('role_id',3);
        return view('franchise.index',compact('students','franchise'));
      }
      elseif(auth()->user()->role_id == "2")
      {
          $students = User::all()->where('id',auth()->user()->parent_id);
          return view('students.index', compact('students','franchise'));
      }
      else {
        $students = User::all()->where('role_id',3);
        return view('franchise.index',compact('students','franchise'));
      }
    }
}
