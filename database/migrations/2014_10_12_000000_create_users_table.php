<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->default(0);
            $table->string('name');
            $table->string('email')->unique();
            $table->string('franchise')->default('none');
            $table->string('district')->default('none');
            $table->string('address')->default("none");
            $table->string('gender')->default("none");
            $table->string('course')->default("none");
            $table->integer('mob_no')->default(0);;
            $table->integer('level')->default(1);
            $table->integer('status')->default(0);
            $table->string('password');
            $table->string('slug')->default('none');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
